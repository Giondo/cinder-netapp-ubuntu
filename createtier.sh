#!/bin/bash
TIER=$1
if [ -z $1 ]; then
	echo "Usage: $0 string"
	echo "Example $0 silver"
else	
    echo "create files using templates..."
    cp -r cinder-netapp-template cinder-netapp-$TIER
    sed -i 's/"name": "cinder-netapp"/"name": "cinder-netapp-'$TIER'"/g' cinder-netapp-$TIER/metadata.yaml
    echo "Deploy local charm"
    juju deploy ./cinder-netapp-$TIER
    echo "applying config"
    juju config cinder-netapp-$TIER netapp-login=USERNAME netapp-password=PASSWORD netapp-server-hostname=NETAPPHOSTNAME netapp-server-port=80 netapp-storage-family=ontap_cluster netapp-storage-protocol=iscsi netapp-transport-type=http netapp-vserver=NETAPPVSERVER netapp_volume_list=netapp_${TIER}_1 volume-backend-name=netapp-$TIER
    echo "Add relation to cinder in order to apply the config"
    juju add-relation cinder-netapp-${TIER} cinder
    echo "-------------------------------------------------------------------------------"
    echo "Please change the volumes according to the list provided by the Netapp Admin"
    echo "example: juju config cinder-netapp-$TIER netapp_volume_list=\"netapp_${TIER}_1, netapp_${TIER}_2\""
    echo "-------------------------------------------------------------------------------"
    echo "To monitor the status of the deploy use: "
    echo "juju debug-log -i cinder-netapp-$TIER"
    echo "-------------------------------------------------------------------------------"
    echo "If you need to remove a previous charm you can do juju remove-application cinder-netapp-${TIER}"
    echo "-------------------------------------------------------------------------------"
    echo "In order to mach the tier with a volume type on Openstack you must create a type and set the backend property"
    echo "openstack volume type create netapp-${TIER}"
    echo "openstack volume type set --property volume_backend_name=netapp-${TIER} netapp-${TIER}"
fi
